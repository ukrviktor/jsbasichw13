// Відповіді на питання:

// 1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
// Різниця полягає в тому, що setTimeout() викликає функцію один раз через заданий проміжок часу, а setInterval() викликає функцію регулярно, через заданий проміжок часу.


// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Якщо викликати функцію setTimeout() і передати в неї нульову затримку, то ми показуємо що хочеми виконати функцію, як найшвидше. Але планувальник буде викликати функцію тільки після завершення виконання поточного коду.

// 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// При виклику функції setTimeout()/setInterval() на неї створюється внутрішнє посилання. Це робиться для того щоб вона не потрапила у збирач сміття. І якщо не викликати функцію clearInterval() то наша функція буде залишатися в пам’яті і навантажувати її.



// Завдання №1

const timeToNextImg = 3000;
const nameStyleActive = 'active';

const imgItmes = document.querySelectorAll('.image-to-show');
const btnStopSlider = document.querySelector('.stop');
const btnContinueSlider = document.querySelector('.continue');
const timerText = document.querySelector('.timer');

let imgCurrentItem = 0;
let linkTimer;
let timeContinue = timeToNextImg;

function nextImg() {
    imgCurrentItem =
        imgCurrentItem < imgItmes.length - 1 ? imgCurrentItem + 1 : 0;
    imgItmes.forEach((item) => item.classList.remove(nameStyleActive));
    imgItmes[imgCurrentItem].classList.add(nameStyleActive);
}

function goTimerToNextImg(currentTime) {
    setTextTimer(currentTime);

    currentTime -= 100;
    timeContinue = currentTime;

    if (currentTime <= 0) {
        nextImg();
        clearTimeout(linkTimer);
        goTimerToNextImg(timeToNextImg);
    } else {
        linkTimer = setTimeout(() => {
            goTimerToNextImg(currentTime);
        }, 100);
    }
}

function setTextTimer(time) {
    let text = (time / 1000).toString().split('.');
    timerText.textContent = `${text[0]}s : ${text[1] || '0'}00ms`;
}

btnStopSlider.addEventListener('click', () => {
    clearTimeout(linkTimer);
});

btnContinueSlider.addEventListener('click', () => {
    goTimerToNextImg(timeContinue);
});

btnContinueSlider.click();